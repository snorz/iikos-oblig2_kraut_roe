#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg?"
 echo "  2 - Hvor lenge er det siden sist boot?"
 echo "  3 - Hva var gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tr�der finner? "
 echo "  5 - Hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""


 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    read -r
    clear
    ;;
  2)clear
    echo "Det er $(uptime | awk '{print $1}' ) dager siden sist boot"
    read -r
    clear
    ;;
  3)clear   
    echo "Gjennomsnittlig load siste minuttet er $(uptime | awk '{print $10}')"
    read -r
    clear
     ;;
  4)clear
    echo "Det finnes $( grep processes /proc/stat | awk '{print $2}') prosesser og traader. "
    read -r
    clear  
     ;;
  5)clear
    echo "Det er $(grep /proc/stat ctxt | awk '{print $2}') context switch'er som fant sted siste sekund"
    read -r
    clear   
    ;;
  6)clear
    echo "Det var $(vmstat 1 2 | tail -1 | awk '{print $11}') interrupts paa et sekund."
    read -r
    clear  
    ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done

