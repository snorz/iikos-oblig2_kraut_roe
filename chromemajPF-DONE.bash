#!/bin/bash

app="firefox"

PROG=$(pgrep $app)

echo "$app  "
for i in $PROG
do
    ANT=$(ps --no-headers -o maj_flt "$i")
    echo "prosess $i has $ANT page faults"
    if [[ "$ANT" -ge "1000" ]]; then
        echo " (OVER 1000)" 
    fi
    
done
