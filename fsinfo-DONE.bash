#!/bin/bash

    # dette programmet tar en directory som argument, 
    #   skriver hvor stor del av partisjonen directorien befinner seg på er full 
    #   Hvor mange filer finnes 
    #   Gjennomsnittlig filstorrelse
    #   Full path til storste fil
    #   Hvilken fil (uten directories) har flest hardlinker til seg selv
        
        #finner path til aktuell mappe
    #find . iikos-oblig1_kraut_roe | grep iikos-oblig1_kraut_roe | head -1

read -r inp    # leser innput

#----------------------   UTREGNING  ----------------------------------

path=$(find / "$inp" 2>/dev/null | grep "$inp" | head -1)

#dUsage=$(df . | awk '{print $5}' | tail -1)    #finner disk usage
dUsage=$(df "$path" | awk '{print $5}' | tail -1)    #finner disk usage
                                               # Finner antall filer i mappen 
antFil=$(find "$inp" ! -wholename '*/.*' -type f -printf '%s %p\n' | wc -l)
                                                
storMapp=$(du -s "$inp" | awk '{print $1}')   #Finner storrelsen paa mappen
gjenFilSto=$(( storMapp / antFil  ))      # gjennomsnitt storrelse paa filer

#storsteFil=$(find "$inp" -not -path '*/\.*' -printf '%s %p\n' | sort -nr | head -1 )

storrelseFil=$(find "$inp" ! -wholename '*/.*' -type f -printf '%s %p\n' | sort -nr | head -1 |  awk '{print $1}')

p1=$(pwd)
p2=$(find "$inp" ! -wholename '*/.*' -type f -printf '%s %p\n' | sort -nr | head -1 |  awk '{print $2}')
pathStorst=$p1$p2

        # finner den filen med flest antall hardlinker og printer path + antallet
hl=$(find "$inp" -ls | sort -k 4,4 | tail -1 | awk '{print $4}' )
pHL=$(find "$inp" -ls | sort -k 4,4 | tail -1 | awk '{print $11}' )


#----------------------   OUTPUT  ----------------------------------

echo "Partisjonen $inp befinner seg paa er $dUsage full"
echo "Det finnes $antFil filer her"

echo "Gjennomsnitt storrelse er : $gjenFilSto B"

echo "den storste er $pathStorst  som er $storrelseFil B  stor" 

echo " filen $pHL har flest hardlinker til seg med $hl"

#//////////////////////////////////////////////////////////////////////////////////////////




