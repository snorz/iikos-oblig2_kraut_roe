#!/bin/bash
    # 
    #
    #echo "Eksempel på datoformat: $(date +%Y%m%d-%H:%M:%S)"
    ###################### variables for filename
dt=$(date +%Y%m%d-%H:%M:%S)
d=-
end=.meminfo
    ######################

for a in "$@"   # for all arguments sent 
do
    fname=$a$d$dt$end   # make filename+file
    echo "****** Minne info om prosess med PID $a ******" > "$fname"
                                                        
                                        # printer ut til fil ; tot bruk virtuelt minne
    echo "(a) Totalt bruk av virtuelt minne (VmSize):   $(grep VmSize /proc/"$a"/status | awk '{print $2 $3}')"  >> "$fname"
    
    ##############################################################################################
    vmSize=$(grep VmSize /proc/"$a"/status | awk '{print $2}')
    vmStk=$(grep VmStk /proc/"$a"/status | awk '{print $2}')
    vmExe=$(grep VmExe /proc/"$a"/status | awk '{print $2}')
    privM="$((vmSize+vmStk+vmExe))"  # riktig?                            # samler alle tre i en var
    
    echo "(b) Mengde virtuelt minne som er privat virtuelt minne (VmData+VmStk+VmExe):  $privM KB" >> "$fname"  #skriver det til filen
    ################################################################################################
       
         ## skriver shared virtuelt minne til fil
    vmLib=$(grep VmLib/proc/"$a"/status | awk '{print $2 $3}')
    echo "(c) Mengdevirtuelt minne som er shared (VmLib): $vmLib" >> "$fname"
       
    ################################################################################################ 
   
        ## skriver totalt bruk av fysisk minne 
    vmRSS=$(grep VmRSS /proc/"$a"/status | awk '{print $2 $3}')
    echo "(d) Totalt bruk av fysisk minne (VmRSS): $vmRSS" >> "$fname"
            
    ################################################################################################ 
    
        ## Skriver mengde fysisk minne som benyttes til page table
    mvPTE=$(grep VmPTE /proc/"$a"/status | awk '{print $2 $3}') 
    echo "(e) Mengde fysisk minne som benyttes til pagetable (VmPTE) $mvPTE" >> "$fname"

    ################################################################################################ 

done



